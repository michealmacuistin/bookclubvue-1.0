import Api from '@/services/api'

export default{
  fetchReviews(){
    return Api().get('/reviews')
  },
  giveStar (id) {
    return Api().put(`/reviews/${id}/star`)
  },
  deleteReview (id) {
    return Api().delete(`/reviews/${id}`)
  },
  postReview (review) {
    return Api ().post('/reviews', review,
      { headers: {'Content-type': 'application/json'}
      })
  },
  fetchReview (id) {
    return Api().get(`/reviews/${id}`)
  },
  putReview (id, review) {
    console.log('REQUESTING ' + review._id + ' ' +
      JSON.stringify(review, null, 5))
    return Api().put(`/reviews/${id}`, review,
      { headers: {'Content-type': 'application/json'} })
  }
}
